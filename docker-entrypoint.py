import os
import utils

print(">>>> Getting Job listing url")
url = os.getenv('JOBS_LISTING_URL')
print(">>>> Parse {}".format(url))
utils.fetch(url=url, save=True, single=False)