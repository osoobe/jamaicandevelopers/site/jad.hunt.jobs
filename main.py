#!/usr/bin/env bin/python
# coding: utf-8

import click
import utils

@click.command()
@click.argument('url')
@click.option('--save/--print', default=True, help="Use this option if you do not want to save the job, default is save")
@click.option('--single/--multiple', default=False, help="Use this option if the URL is the job page, default is multiple")
def fetch(url=None, save=True, single=False):
    """Fetch jobs.
    
    Mandatory Arguements:
        url (string):   The target URL. Please ensure it is in quotations. Otherwise, the script will not work
    
    Example:
        bin/python main.py 'https://www.caribbeanjobs.com/ShowResults.aspx?Keywords=&Location=27&Category=3&Recruiter=Company&Recruiter=Agency'
    
    """
    utils.fetch(url=url, save=save, single=single)


if __name__ == '__main__':
    fetch()