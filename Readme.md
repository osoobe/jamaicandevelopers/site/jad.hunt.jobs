# JDG Job Curator

Curators are responsible for assembling, cataloguing and displaying collection of jobs from various industry.

## Usage

#### Get Jobs using Docker.

Run the following command to generate a list of jobs from various sources.
```bash
docker run -t --rm  -v ./jobs:/usr/src/app/data jamdev/job-curator
```

Aftwards, if you run the following command, you should see a new JSON file in the `jobs` folder
```
ls -al ./jobs/
```