import base64
import json
import time
import requests
import vocab
from bs4 import BeautifulSoup as bs4
from datetime import datetime
from glob import glob


def get_as_base64(url):
    return base64.b64encode(requests.get(url).content)


def parse_carib_job(job_url, job_id=None):

    print(">>>> Processing Job url: {}".format(job_url))
    website = vocab.carib_job_domain
    r_job = requests.get(job_url)
    job_page = bs4(r_job.text, 'html.parser')
    job_content = job_page.select_one('div.full-page.three-quarters > div > div.module-content > div.job-description')
    job_description = job_page.select_one('meta[property="og:description"]')['content']
    image_url = "{}/{}".format(website.strip("/"), job_content.select_one('.logo-mob img')['src'].strip('/'))
    job_apply_url = "{}/{}".format(website.strip("/"), job_content.select_one('a.apply-now')['href'].strip('/'))
    
    if 'not-disclosed' in image_url:
        image_url = 'https://i.ibb.co/jH1FvCB/no-photo-available-icon-4.png'
        
    if not job_id:
        job_id = job_content.select_one('.save-job')['jobid']
    
    print(">>>> Build Job {} data".format(job_id))
    
    return dict(
        job_title=job_content.select_one('h1').text,
        job_location=job_content.select_one("li.location").text,
        job_employment_type=job_content.select_one("li.employment-type").text,
        job_employment_availability="Non Disclosure",
        job_details=job_content.select_one("div.job-details").prettify(),
        job_apply_url=job_apply_url,
        image_url=image_url,
        job_url=job_url,
        job_provider='caribbeanjobs',
        job_id=job_id,
        job_description=job_description
    )


def parse_carib_jobs(url, save=True):
    print(">>>> Process {}".format(url))
    r = requests.get(url)
    soup = bs4(r.text, 'html.parser')
    website = vocab.carib_job_domain

    jobs = soup.select('div.job-result-title > h2 > a')
    limit = 10
    try:
        with open('skip_jobs.json', 'r') as fp:
            skip_jobs = json.load(fp)
    except:
        skip_jobs = {
            'caribjobs': []
        }
    parsed_jobs = []
    num_jobs = len(jobs)
    print(">>>> Found {} jobs".format(num_jobs))
    for index, job in enumerate(jobs):
        job_id = job['jobid']
        if index == limit:
            break
        if job_id in skip_jobs['caribjobs']:
            print(">>>> Skip Job {}".format(job_id))
            continue
        job_url = "{}/{}".format(website.strip("/"), job['href'].strip("/"))
        job_data = parse_carib_job(job_url, job_id)
        parsed_jobs.append(job_data)
        skip_jobs['caribjobs'].append(job_id) 

    if save:
        if len(parsed_jobs) > 0:
            with open('skip_jobs.json', 'w') as fp:
                json.dump(skip_jobs, fp)
        
            now_str = datetime.now().strftime("%m-%d-%Y-%H-%M-%S")
            filename = 'data/parsed_jobs_{}.json'.format(now_str)
            print(">>>>> Saving Job {} data to {}".format(
                job_id,
                filename
            ))
            with open(filename, 'w') as fp:
                json.dump(parsed_jobs, fp)

    return parsed_jobs


def save_job(url):
    try:
        with open('skip_jobs.json', 'r') as fp:
            skip_jobs = json.load(fp)
    except:
        skip_jobs = {
            'caribjobs': []
        }
    job_data = parse_carib_job(url)
    job_id = job_data['job_id']
    if job_id in skip_jobs:
        return False
    
    skip_jobs['caribjobs'].append(job_id)
    with open('skip_jobs.json', 'w') as fp:
        json.dump(skip_jobs, fp)

    now_str = datetime.now().strftime("%m-%d-%Y-%H-%M-%S")
    filename = 'data/parsed_jobs_{}.json'.format(now_str)
    print(">>>>> Saving Job {} data to {}".format(
        job_id,
        filename
    ))
    with open(filename, 'w') as fp:
        json.dump([job_data], fp)



def fetch(url=None, save=True, single=False):
    """Fetch jobs.
    
    Mandatory Arguements:
        url (string):   The target URL. Please ensure it is in quotations. Otherwise, the script will not work
    
    Example:
        bin/python main.py 'https://www.caribbeanjobs.com/ShowResults.aspx?Keywords=&Location=27&Category=3&Recruiter=Company&Recruiter=Agency'
    
    """
    if not single:
        result = parse_carib_jobs(url, save=save)
        if not save:
            print(result)
    elif save:
        save_job(url)
    else:
        print(parse_carib_job(url))

    files = glob("data/*")
    print(">>>>> Available files")
    for file in files:
        print("---> {}".format(file))
