FROM python:2.7.17-alpine

WORKDIR /usr/src/app
COPY . /usr/src/app

ENV JOBS_LISTING_URL="https://www.caribbeanjobs.com/ShowResults.aspx?Keywords=&Location=27&Category=3&Recruiter=Company&Recruiter=Agency"

RUN pip install --no-cache-dir -r requirements.txt

VOLUME [ "/usr/src/app/data" ]

CMD ["python", "docker-entrypoint.py"]
